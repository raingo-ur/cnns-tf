#!/bin/bash
# vim ft=sh

#../../../cnns/analysis/fetch.sh animal gpu-sc:/raid/yli/ner.tf/models/datasets/animal/data/exps #gpu-cs:/run/media/yli/yli-data/gpu-cs/imagenet/animal-v2/exps/ gpu-cs2:/run/media/yli/yli-data2/gpu-cs2/tf-data/animal-v2/exps/

cwd=`readlink -f $PWD`

this_dir=`readlink -f $0`
this_dir=`dirname $this_dir`
if [ ! -f plots.twb ]
then
  cat $this_dir/plots.twb | sed "s#DIRECTORY#$cwd#" > plots.twb
fi

target=$cwd/all.tsv
rm -f $target

for exp in `cat _arxiv`
do
  mkdir -p $exp
  pushd $exp
  for entities in `ls $cwd/../entities/*.entities`
  do
    name=`basename $entities`
    name=${name%.*}
    ln -sf $entities .
    $CNNS/analysis/fetch.sh $name $exp
    if [ -f $target ]
    then
      cat $name.txt | tail -n +2 >> $target
    else
      cat $name.txt > $target
    fi
  done
  popd
done
