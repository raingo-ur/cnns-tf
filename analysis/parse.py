#!/usr/bin/env python
"""
parse json result files, dump to txt file for tableau
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os
import os.path as osp
import json

from collections import defaultdict

def _is_num(value):
  return isinstance(value, (int, long, float, complex))

def _parse_json_path(json_path):
  name = osp.dirname(json_path)
  names = name.split('/')
  exp = names[2:]
  names = ['/'.join(names[:2]), '/'.join(exp[::-1])]
  return names


def _load_vocab():
  vocab = {}
  try:
    with open(sys.argv[1]) as reader:
      for idx, line in enumerate(reader):
        vocab[idx] = line.strip()
  except Exception as ioe:
    print(str(ioe), file=sys.stderr)
    pass

  def _vocab(x):
    if x in vocab:
      return vocab[x]
    else:
      return x

  return _vocab, len(vocab)

def main():
  vocab, vocab_size = _load_vocab()

  head = ['dataset', 'exp', 'iteration', 'split', 'metric', 'idx', 'score']
  print(*head, sep='\t')

  for json_path in sys.stdin:
    json_path = json_path.strip()
    json_name = osp.basename(json_path)

    split = json_name.split('.')[-3]
    names = _parse_json_path(json_path)

    def _handle_list(metric, scores, iteration):

      def _get_idx(idx):
        if vocab_size > 0 and len(scores) >= vocab_size * vocab_size:
          row = idx // vocab_size
          col = idx % vocab_size
          return vocab(row) + ':' + vocab(col)
        else:
          return vocab(idx)

      for idx, score in enumerate(scores):
        if not _is_num(score):
          continue
        fields = names + [iteration, split, metric, _get_idx(idx), score*100]
        print(*fields, sep='\t')

    def _handle_dict(values, new_values, prefix=""):
      for key, scores in values.items():
        new_p = prefix + '-' + key if len(prefix) > 0 else key
        if isinstance(scores, dict):
          _handle_dict(scores, new_values, new_p)
        else:
          new_values[new_p] = scores

    reader = open(json_path)
    loss = []
    with open(json_path) as reader:
      res = json.load(reader)

      for iteration, values in res.items():
        if isinstance(values, dict):
          values = values['info']
        else:
          loss.append((iteration, values))
          continue

        new_values = {}
        _handle_dict(values, new_values)
        values = new_values

        for metric, scores in values.items():
          if not isinstance(scores, list):
            scores = [scores]
          _handle_list(metric, scores, iteration)

          if metric.endswith('P-P'):
            pcr = values[metric.replace('P-P', 'R-P')]
            F1 = [p*r/(p+r) if p+r > 0 else 0 for p, r in zip(scores,pcr)]
            _handle_list(metric.replace('P-P', 'F1-P'), F1, iteration)

    if len(loss) > 0:
      with open(json_path + '.loss.txt', 'w') as writer:
        loss.sort()
        for i, l in loss:
          print(i, l, file=writer)

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
