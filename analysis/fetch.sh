#!/bin/bash
# vim ft=sh

#exp_dirs=(gpu-cs:/run/media/yli/yli-data/gpu-cs/imagenet/animal-v2/exps/ gpu-cs2:/run/media/yli/yli-data2/gpu-cs2/tf-data/animal-v2/exps/)

if [ $# -lt "2" ]
then
  echo $0 local-folder exp-name
  exit
fi

save_dir=$1
exp=$2
mkdir -p $save_dir

echo $exp/$save_dir
gsutil -m rsync -r -x ".*tempstate.*|.*jpg$|.*snapshot$|.*npz$|.*pb.*|.*model.ckpt$|.*data.*|.*jtree.pb.*|.*guides.*|.*events.*" "$GCS_ROOT/$exp/$save_dir/" $save_dir

this_dir=`dirname $0`
find $save_dir -name '*.json' | python $this_dir/parse.py $save_dir.entities > $save_dir.txt
