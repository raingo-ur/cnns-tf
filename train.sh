#!/bin/bash
# vim ft=sh
set -e

exp_dir=$1
shift
gpu=$1
shift
args=$@

this_dir=`dirname $0`
real_this_dir=`readlink -f $0`
real_this_dir=`dirname $real_this_dir`

flag=$exp_dir/FINISHED
if [ -f $flag ]
then
  exit
fi

if [ "$resp" == "-1" ]
then
  exit
fi

mkdir -p $exp_dir

git rev-parse HEAD > $exp_dir/COMMIT
ckpt_path=$exp_dir/model.ckpt
ckpt_path=`readlink -f $ckpt_path`

hostname=`hostname -s`
mach=$hostname-$gpu-$$
echo $mach >> $exp_dir/COMMIT

function _run {
  split=$1
  if [ -f "$TC_MALLOC" ]
  then
    export LD_PRELOAD=$TC_MALLOC
    echo using tcmalloc: $TC_MALLOC
  else
    echo tcmalloc is not found
  fi
  echo Running $exp_dir ..
  CUDA_VISIBLE_DEVICES=$gpu $_CNNS_LAUCHER python \
    $this_dir/main.py \
    --eval_split $split.tf \
    --ckpt_path $ckpt_path \
    `$real_this_dir/_get_cmd.sh $exp_dir` $args
}

_run dev

for arg in ${args[@]}
do
  if [ "$arg" == "--eval_split" ]
  then
    exit
  fi
done

set +e
do_ckpt2graph=`$real_this_dir/_get_cmd.sh $exp_dir | grep convert_graph_after_done`
if [ ! -z "$do_ckpt2graph" -a ! -f $ckpt_path.pb ]
then
  $this_dir/ckpt2graph.sh $ckpt_path
fi

eval_on_train=`$real_this_dir/_get_cmd.sh $exp_dir | grep eval_outputs_path`
if [ ! -z "$eval_on_train" ]
then
  _run train
fi
set -e
_run test
touch $flag

$real_this_dir/arxiv.sh $exp_dir yes
