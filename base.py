#!/usr/bin/env python
"""
testing the cnn nets

__init__ with images
init to load weights
features to access the cnn features tensor
logits to access the imagenet logits
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os
import tensorflow as tf
import numpy as np
from . import optim

from collections import namedtuple

this_dir = osp.dirname(osp.realpath(__file__))
testdata_dir = osp.join(this_dir, 'testdata')

class TestCNN(object):
  """
  test cases for the cnn modules
  """
  _CNN = None
  batch_size = 2
  num_output = 500

  @classmethod
  def setUpClass(self):
    pass

  def _images(self):
    return tf.random_normal(
        [self.batch_size, self._CNN.input_image_size,
          self._CNN.input_image_size, self._CNN.channels],
        dtype= tf.float32,
        name = 'images')

  def _cnn(self):
    with tf.variable_scope("imagenet"):
      return self._CNN(self._images())

  def _get_heads(self):

    heads = {}
    heads['logits'] = (self.batch_size, self._CNN.imagenet_num_classes)
    heads['features'] = (self.batch_size, self._CNN.feature_dim)

    return heads.items()

  def test_shape(self):
    cnn = self._cnn()

    for head, shape in self._get_heads():
      shape = getattr(cnn, head).get_shape()
      for i, s in enumerate(shape):
        self.assertEqual(shape[i], s)

  def test_trainable(self):
    cnn = self._cnn()
    ts = cnn.trainables()
    self.assertEqual(len(ts), 2)

  def test_init(self):
    cnn = self._cnn()
    with self.test_session() as sess:
      cnn.init(sess)
      for head, shape in self._get_heads():
        data = sess.run(getattr(cnn, head))
        self.assertEqual(data.shape, shape)

  def _test_panda(self, do_distort):
    if not self._CNN.pretrained:
      return

    image_dir = osp.join(testdata_dir, 'synset_images')
    with tf.variable_scope("panda-%d" % do_distort):
      label, path, cnn = self._CNN.classify_jpeg(do_distort)
    with self.test_session() as sess:
      cnn.init(sess)
      success = []
      for name in os.listdir(image_dir):
        if name.endswith('.jpg'):
          image_path = osp.join(image_dir, name)
          synset = osp.splitext(name)[0]
          predicted = sess.run(label, feed_dict = {path:image_path})[0]
          success.append(cnn.synsets[predicted] == synset)
      self.assertLess(0.5, sum(success)/len(success))

  def test_panda_eval(self):
    self._test_panda(False)

  def test_panda_train(self):
    self._test_panda(True)

FeatureMap = namedtuple('FeatureMap', ['layer', 'dim', 'size'])

class CNN(object):
  """
  base class for the CNN modules
  defines common behaviors and the interface
  """
  channels = 3
  flip = True
  central_fraction = 0.875
  def __init__(self, cnn_dir):
    self._load_synsets(cnn_dir)

  # filter out variables related to the current cnn
  def filter_variables(self, var_list):
    return [var for var in var_list if self.strip_scope(var.name) is None]

  def strip_scope(self, scope):
    if ':' in scope:
      scope = scope[:scope.find(':')]
    prefix = self.scope.name + '/'
    if prefix in scope:
      return scope[(scope.find(prefix)+len(prefix)):]
    else:
      return None

  def _load_synsets(self, model_dir):
    synset_path = osp.join(model_dir, 'synsets.txt')
    self.synsets = []
    with open(synset_path) as reader:
      for line in reader:
        self.synsets.append(line.strip())

  def init(self, sess):
    raise NotImplemented

  def trainables(self, train_cnn):
    raise NotImplemented

  def setup_decay(self, name='weights'):
    # TODO fix this hacky
    weights = [var for var in self.all_trainables
        if var.name.split(':')[0].endswith('weights')]
    optim.set_var_decay(weights, 1.)

  @classmethod
  def classify_jpeg(CNN, do_distort=False):
    image_path = tf.placeholder(tf.string)
    encoded = tf.read_file(image_path)
    jpeg = tf.image.decode_jpeg(encoded, channels=3)
    image = CNN.preprocess(jpeg, do_distort)
    image = tf.expand_dims(image, 0)
    with tf.variable_scope("classify"):
      cnn = CNN(image, is_train=False)
    label = tf.argmax(cnn.logits, 1)
    return label, image_path, cnn

  @staticmethod
  def _distort_color(image):
    image = tf.image.random_brightness(image, max_delta=32. / 255.)
    image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
    image = tf.image.random_hue(image, max_delta=0.2)
    image = tf.image.random_contrast(image, lower=0.5, upper=1.5)

    image = tf.clip_by_value(image, 0.0, 1.0)
    return image

  @classmethod
  def _distort_image(cls, image, bbox=None):
    if bbox is None:
      bbox = np.zeros((1, 0, 4))
    bbox = tf.image.sample_distorted_bounding_box(
        tf.shape(image), bounding_boxes=bbox,
        area_range=[0.85, 1.0],
        use_image_if_no_bounding_boxes=True)

    begin, size, _ = bbox

    # Crop the image to the specified bounding box.
    distorted_image = tf.slice(image, begin, size)

    height = cls.input_image_size
    width = cls.input_image_size

    distorted_image = tf.image.resize_images(distorted_image,
        [height, width], 0)

    # Restore the shape since the dynamic slice based upon the bbox_size loses
    # the third dimension.
    distorted_image.set_shape([height, width, 3])

    # Randomly flip the image horizontally.
    if cls.flip:
      distorted_image = tf.image.random_flip_left_right(distorted_image)

    # Randomly distort the colors.
    # distorted_image = cls._distort_color(distorted_image)

    return distorted_image

  @classmethod
  def _eval_image(cls, image):
    if cls.central_fraction < 1.:
      image = tf.image.central_crop(image,
          central_fraction=cls.central_fraction)
    image = tf.image.resize_images(image,
        [cls.input_image_size,
        cls.input_image_size], 0)
    return image

  @classmethod
  def preprocess(cls, image, is_train=False):
    """
    preprocess an image
    input image is arbitrary size with channels=channels
    in the range of [0, 1)
    output image will be [input_image_size, input_image_size, channels]

    mean substracted
    scaled
    """
    # all tensorflow preprocessing routines needs to be in 0,1 range
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)
    if is_train:
      image = cls._distort_image(image)
    else:
      image = cls._eval_image(image)
    return cls._rescale(image)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
