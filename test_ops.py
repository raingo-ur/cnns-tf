#!/usr/bin/env python
"""
test optim module
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import tensorflow as tf
import numpy as np
from . import ops

class OpsTest(tf.test.TestCase):

  def setUp(self):
    super(OpsTest, self).setUp()

  def test_shape(self):
    a = tf.constant(np.arange(1,10), shape=[1, 3, 3, 1], dtype=tf.float32)
    w = tf.constant(np.arange(1,17), shape=[2, 2, 4, 1], dtype=tf.float32)

    with self.test_session() as sess:

      output = ops.locally_connected(a, 2, 1, bias=False, weights=w).eval()
      gt = np.array([[37,111],[259,413]], dtype=np.float32).reshape([1,2,2,1])
      self.assertAllEqual(output, gt)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
