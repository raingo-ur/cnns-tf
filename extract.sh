#!/bin/bash
# vim ft=sh

exp_dir=$1
tf_dir=$2
gpu=$3

this_dir=`dirname $0`
data_dir=`dirname $tf_dir`
split=`basename $tf_dir`


if [ ! -f "$exp_dir/FINISHED" ]
then
  echo "$exp_dir" is not finished, not ready for extract
  exit
fi
rm $exp_dir/FINISHED
$this_dir/train.sh $exp_dir $gpu --data_dir $data_dir --eval_split $split --eval_outputs_path extract --eval_only
touch $exp_dir/FINISHED

save_dir=$data_dir/$split.extract/$exp_dir/
mkdir -p $save_dir
cp $exp_dir/configs $save_dir/
mv $exp_dir/*$split* $save_dir/
