#!/usr/bin/env python
"""
support functions for data visualization
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from PIL import Image
import StringIO
import traceback

import os
import fnmatch

def argsort(scores, sign=1):
  idx_delta = [(idx, score)
      for idx, score in enumerate(scores)]
  idx_delta.sort(key = lambda x:sign*x[1])
  return [f[0] for f in idx_delta]

def iglob(target, pat):

  for root, dirnames, filenames in os.walk(target):
    for filename in fnmatch.filter(filenames, pat):
      yield osp.join(root, filename)

def trace_on_fail(func):
  def wrap(*args):
    try:
      return func(*args)
    except Exception as ioe:
      return "<pre>" + traceback.format_exc() + "</pre>"
  return wrap

def img2base64(x):
  output = StringIO.StringIO()
  x.save(output, "PNG")
  contents = output.getvalue().encode("base64").replace('\n', '')
  output.close()
  return "data:image/png;base64," + contents

def imread(path):
  return Image.open(path).convert('RGB')

def load_img(path, size=256):
  x = imread(path).resize((size,size))
  return img2base64(x)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
