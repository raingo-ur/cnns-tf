
# CNN based framework

Refer to one of the following projects for usage
1. image-caption
2. multi-label
3. knowledge-base

To set up a new model,
1. `compile_data.py` needs to know how to parse txt file with `parse_line` and how to convert the txt file into tf records `convert_to_example`
2. `inputs.py` needs `parse_example` output dictionary
3. `main.py` to define train and eval models
4. `utils.py` for `compile_data` and `inputs`. mainly `utils.inputs` and `utils._compile_main`

## interface
1. `ops.py` defines simple operations
2. `runner.py` defines the main control logic
3. `base.py` defines an interface for CNN
4. `optim.py` defines a general optimization framework

## `compile_data.py`
convert a txt file into tf records, and the txt file must be space seperated

define two functions to parse txt file and save tf records

`parse_line` will take list of fields and return a tuple

`convert_to_example` take the tuple and convert to a `tf.train.Example`

## `inputs.py`
parse the tf records into dictionary of tensors

`_parse_example_proto` will take a string tensor, return a dictionary of tensors

call `cnns.utils.inputs` to get dictionary of batched tensors

## `main.py`
define train and eval logic, define `get_iter` function

`get_iter` take the split string, returns `epoch_size`, a function, or optionally `cnn` objects.

for training phase, the function should return the loss value upon each call

for evaluation, the function return a dictionary and a score. The score is used to determine which model to save

if pre load cnn weights is needed, the `cnn` object should be returned as well, but that's optional. Required iff `FLAGS.load_cnn_weights` is set

## toy example

the image generation facility and label info is inclueded, but specific model need additonary scripts to construct real toy data

## how to do weight decay
add `(variables, decay multiplier)` into VAR_DECAY collection

## how to do variable learning rate
add `(variables, lr multiplier)` into VAR_LR collection

## add a new CNN module

1. `__init__.py` to make directory importable
2. `net.py` defines the Net, inherited from base.CNN
3. `test.py` defines the test, simple file, inherited from base.TestCNN
4. `fetch.sh` to download resources and setup
5. `.gitignore` to ignore the resources
6. `synsets.txt` is the index of the synsets

## run test
`python -m unittest discover`
