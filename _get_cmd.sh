#!/bin/bash
# vim ft=sh

#### Hiearcy config system: use everything named config or _config will be picked up into final configs and saved in the configs

exp_dir=$1
org_exp_dir=$exp_dir

cmd=""
function _cfg {
  config_path=$1
  if [ -f $config_path ]
  then
    cmd=`cat $config_path | grep -v '#' | tr '\n' ' '`" $cmd"
  fi
}

cmd=""
while [ ${#exp_dir} -gt 1 ]
do
  _cfg $exp_dir/config
  _cfg $exp_dir/_config
  exp_dir=`dirname $exp_dir`
done

_cfg ./_config
_cfg ./_local.config

echo $cmd | tee $org_exp_dir/configs
