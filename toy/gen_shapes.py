#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os
import os.path as osp
from PIL import Image, ImageDraw
import numpy as np

IMG_SIZE = 28
min_size = 15

class BBox(object):
  def __init__(self):
    arr = self._get_rand()
    self.x0 = arr[0]
    self.x1 = arr[1]

    arr = self._get_rand()
    self.y0 = arr[0]
    self.y1 = arr[1]

  def _get_rand(self):
    arr = np.sort(np.random.randint(0, IMG_SIZE, size=(2)))
    while arr[1] - arr[0] < min_size:
      arr = np.sort(np.random.randint(0, IMG_SIZE, size=(2)))
    return arr

  def as_tuple(self):
    return (self.x0, self.y0, self.x1, self.y1)

  @property
  def tl(self):
    return (self.x0, self.y0)

  @property
  def tr(self):
    return (self.x1, self.y0)

  @property
  def bl(self):
    return (self.x0, self.y1)

  @property
  def br(self):
    return (self.x1, self.y1)

  @property
  def top(self):
    return (self.x0+(self.x1-self.x0)//2, self.y0)

  @property
  def bottom(self):
    return (self.x0+(self.x1-self.x0)//2, self.y1)

  @property
  def left(self):
    return (self.x0, self.y0+(self.y1-self.y0)//2)

  @property
  def right(self):
    return (self.x1, self.y0+(self.y1-self.y0)//2)

  @property
  def width(self):
    return self.x1 - self.x0

  @property
  def height(self):
    return self.y1 - self.y0

  def subset(self, ratio):
    # get the best fit sub region with the w:h ratio
    # rect is numpy array: x0, y0, x1, y1
    height = self.height
    width = self.width

    res_w = height * ratio
    res_h = width / ratio
    if res_w < width:
      delta = (width-res_w)//2
      delta = (delta, 0, -delta, 0)
    elif res_h < height:
      delta = (height-res_h)//2
      delta = (0, delta, 0, -delta)
    else:
      delta = (0, 0, 0, 0)

    res = BBox()
    res.x0 = self.x0 + delta[0]
    res.y0 = self.y0 + delta[1]
    res.x1 = self.x1 + delta[2]
    res.y1 = self.y1 + delta[3]

    return res

  def __str__(self):
    return '%d %d %d %d' % (self.x0, self.y0, self.x1, self.y1)

class Shape(object):
  def __init__(self):
    self.base = np.ones((IMG_SIZE, IMG_SIZE, 3), dtype=np.uint8)

  def render(self):
    color = np.random.randint(0, 256, size=(1, 1, 3))
    self.base[:] = color

    canvas = Image.fromarray(self.base)
    fill = np.random.randint(0, 256, size=(3)).tolist()
    fill = tuple(fill)

    self._draw(ImageDraw.Draw(canvas), BBox(), fill)
    return canvas

class Rect(Shape):
  def _draw(self, canvas, rect, fill):
    # w:h
    ratio = self._ratio()
    canvas.rectangle(rect.subset(ratio).as_tuple(), fill=fill)

class RectV(Rect):
  name = '4side-verticle'
  def _ratio(self):
    return .3

class RectH(Rect):
  name = '4side-horizon'
  def _ratio(self):
    return 3

class RectS(Rect):
  name = '4side-square'
  def _ratio(self):
    return 1

class Ellipse(Shape):
  def _draw(self, canvas, rect, fill):
    ratio = self._ratio()
    canvas.ellipse(rect.subset(ratio).as_tuple(), fill=fill)

class Chord(Shape):
  def _draw(self, canvas, rect, fill):
    start, end = self._angles()
    canvas.chord(rect.subset(1).as_tuple(), start, end, fill=fill)

class ChordUR(Chord):
  name = 'chord-upright'
  def _angles(self):
    return 270, 360

class ChordUL(Chord):
  name = 'chord-upleft'
  def _angles(self):
    return 180, 270

class ChordDR(Chord):
  name = 'chord-downright'
  def _angles(self):
    return 0, 90

class ChordDL(Chord):
  name = 'chord-downleft'
  def _angles(self):
    return 90, 180

class Sector(Shape):
  def _draw(self, canvas, rect, fill):
    start, end = self._angles()
    canvas.pieslice(rect.subset(1).as_tuple(), start, end, fill=fill)

class QuadUR(Sector):
  name = 'quadrant-upright'
  def _angles(self):
    return 270, 360

class QuadUL(Sector):
  name = 'quadrant-upleft'
  def _angles(self):
    return 180, 270

class QuadDR(Sector):
  name = 'quadrant-downright'
  def _angles(self):
    return 0, 90

class QuadDL(Sector):
  name = 'quadrant-downleft'
  def _angles(self):
    return 90, 180

class SemiUp(Sector):
  name = 'semicircle-up'
  def _angles(self):
    return 180, 360

class SemiDown(Sector):
  name = 'semicircle-down'
  def _angles(self):
    return 0, 180

class EllipseV(Ellipse):
  name = 'ellipse-verticle'
  def _ratio(self):
    return .3

class EllipseH(Ellipse):
  name = 'ellipse-horizon'
  def _ratio(self):
    return 3

class Circle(Ellipse):
  name = 'circle'
  def _ratio(self):
    return 1

class Trig(Shape):
  def _draw(self, canvas, rect, fill):
    path = self._path(rect.subset(1))
    canvas.polygon(path, fill=fill)

class TrigLeft(Trig):
  name = '3side-left'
  def _path(self, rect):
    return [rect.tr, rect.left, rect.br]

class TrigRight(Trig):
  name = '3side-right'
  def _path(self, rect):
    return [rect.tl, rect.right, rect.bl]

class TrigUp(Trig):
  name = '3side-up'
  def _path(self, rect):
    return [rect.bl, rect.top, rect.br]

class TrigDown(Trig):
  name = '3side-down'
  def _path(self, rect):
    return [rect.tl, rect.bottom, rect.tr]

def main():

  num_per_class = 10
  try:
    num_per_class = int(sys.argv[1])
  except:
    pass

  from itersubs import itersubclasses

  for shape in itersubclasses(Shape):
    if hasattr(shape, 'name'):
      save_dir = osp.join('data/imgs', shape.name)
      if not osp.exists(save_dir):
        os.makedirs(save_dir)
      shape_ = shape()
      for i in range(num_per_class):
        shape_.render().save(osp.join(save_dir, '%05d.png' % i))

  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
