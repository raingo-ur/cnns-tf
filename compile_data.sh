#!/bin/bash
# vim ft=sh

this_dir=`dirname $0`
python $this_dir/compile_data.py --txt_path $1 `cat _config | tr '\n' ' '`
