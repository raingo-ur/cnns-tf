#!/usr/bin/env python
"""
utilities shared by many models
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
from . import runner
from time import gmtime, strftime

MAX_IM_S = 318

from cStringIO import StringIO
from PIL import Image
from collections import defaultdict
from .runner import get_pbar

import glob
import math
import random


# inputs_device
tf.app.flags.DEFINE_string('inputs_device', "",
    """inputs pipeline device spec. empty or cpu""")

# num_per_shards
tf.app.flags.DEFINE_integer('num_per_shards', 500,
    """the number of records per tf file""")

# data_dir
tf.app.flags.DEFINE_string('data_dir', './data/',
    """the data directory, containing the tf records""")

# debug_inputs
tf.app.flags.DEFINE_boolean('debug_inputs', False,
    "whether to debug inputs speed by turn it off")

# txt_path
tf.app.flags.DEFINE_string('txt_path', "",
    "txt_path to compile")

FLAGS = tf.app.flags.FLAGS

def ensure_jpeg(path, handle = lambda x:x):
  image_buffer = open(path, 'r')

  img = Image.open(image_buffer).convert('RGB')
  img = handle(img)

  w, h = img.size
  new_size = None
  if w <= h and w > MAX_IM_S:
    new_size = (MAX_IM_S, int(h/w*MAX_IM_S))
  elif w > h and h > MAX_IM_S:
    new_size = (int(w/h*MAX_IM_S), MAX_IM_S)

  if new_size is not None:
    img = img.resize(new_size)

  buffer = StringIO()
  img.save(buffer, format='jpeg')
  res = buffer.getvalue()

  return res

def int64_feature(value):
  """Wrapper for inserting int64 features into Example proto."""
  if not isinstance(value, list):
    value = [value]
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def float_feature(value):
  """Wrapper for inserting float features into Example proto."""
  if not isinstance(value, list):
    value = [value]
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))

def bytes_feature(value):
  """Wrapper for inserting bytes features into Example proto."""
  if not isinstance(value, list):
    value = [value]
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

def process_threads(tid, num_threads,
    data, save_dir,
    _cvt_example,
    name='tf'):
  cnt = 0

  if tid == 0:
    pbar = get_pbar(len(data))

  for idx in range(tid, len(data), num_threads):
    if cnt % FLAGS.num_per_shards== 0:
      output_file = osp.join(save_dir,
          '%s-t%02d-s%05d' % (name, tid, cnt/FLAGS.num_per_shards))
      writer = tf.python_io.TFRecordWriter(output_file)

    try:
      example = _cvt_example(data[idx])
    except Exception as ioe:
      print(ioe, file=sys.stderr)
    else:
      writer.write(example.SerializeToString())
      cnt += 1

    if tid == 0:
      pbar.update(idx)

import threading
import shutil
import os

NUM_THREADS = 4
def _compile_main(_cvt_example, _parse_line):
  txt_path = FLAGS.txt_path
  print(txt_path)
  txt_name = osp.splitext(txt_path)[0]
  save_dir = txt_name + '.tf'

  if osp.exists(save_dir):
    shutil.rmtree(save_dir)
  os.mkdir(save_dir)

  data = []
  with open(txt_path) as reader:
    for line in reader:
      fields = line.strip().split()
      item = _parse_line(fields)
      if item is None:
        continue
      data.append(item)

  random.shuffle(data)

  # Create a mechanism for monitoring when all threads are finished.
  coord = tf.train.Coordinator()

  threads = []
  for thread_index in range(NUM_THREADS):
    args = (thread_index, NUM_THREADS, data, save_dir, _cvt_example)
    t = threading.Thread(target=process_threads, args=args)
    t.daemon = True
    t.start()
    threads.append(t)

  # Wait for all the threads to terminate.
  coord.join(threads)

def inputs(is_train, split, parse_example, batch_size, _CNN=None):
  with tf.device(FLAGS.inputs_device):
    num_preprocess_threads = 4
    tf_dir = osp.join(FLAGS.data_dir, split)

    # setup random shuffle queue
    # shuffle protobin only
    if is_train:
      min_after_dequeue = FLAGS.num_per_shards * 3
      examples_queue = tf.RandomShuffleQueue(
          capacity=min_after_dequeue + 3 * batch_size,
          min_after_dequeue=min_after_dequeue,
          dtypes=[tf.string])
    else:
      min_after_dequeue = FLAGS.num_per_shards
      examples_queue = tf.FIFOQueue(
          capacity=min_after_dequeue + 3 * batch_size,
          dtypes=[tf.string])

    files = get_files(osp.join(FLAGS.data_dir, split))
    num_files = len(files)

    groups = defaultdict(list)
    for f in files:
      ext = osp.splitext(f)[-1]
      groups[ext].append(f)

    enqueue_ops = []
    for g, files in groups.items():
      print(split, 'group', g, len(files))
      filename_queue = tf.train.string_input_producer(files, shuffle=is_train, name='group-' + g)
      reader = tf.TFRecordReader()
      _, value = reader.read(filename_queue)
      enqueue_ops.append(examples_queue.enqueue([value]))

    tf.train.queue_runner.add_queue_runner(
        tf.train.queue_runner.QueueRunner(examples_queue, enqueue_ops))
    example_serialized = examples_queue.dequeue()

    outputs = []
    for _ in range(num_preprocess_threads):
      data = parse_example(example_serialized)

      if _CNN is not None:
        for k, v in data.items():
          if 'image' in k:
            data[k] = _CNN.preprocess(v, is_train)

      keys = data.keys()
      outputs.append(data.values())

    res = tf.train.batch_join(
        outputs,
        batch_size=batch_size,
        capacity=2 * num_preprocess_threads * batch_size)

    epoch_size = num_files * FLAGS.num_per_shards / batch_size
    epoch_size = math.ceil(epoch_size)
    epoch_size = int(epoch_size)

    res_d = {}
    for key, value in zip(keys, res):
      if FLAGS.debug_inputs:
        with tf.device(""):
          res_d[key] = tf.ones(value.get_shape().as_list(),
              dtype=value.dtype)
      else:
        res_d[key] = value

    return res_d, epoch_size

def get_files(target):
  return sorted(glob.glob(osp.join(target, 'tf-*')))

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
