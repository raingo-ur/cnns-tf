#!/usr/bin/env python
"""
test optim module
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import tensorflow as tf
from . import optim
FLAGS = tf.app.flags.FLAGS

def _get_graph():
  x = tf.Variable([.1])
  y = tf.Variable([.1])
  k = tf.Variable([.1])
  z = x*x + y*y + k*10
  return  x, y, z, k

class OptimTest(tf.test.TestCase):

  def setUp(self):
    super(OptimTest, self).setUp()
    FLAGS.base_lr = .1
    FLAGS.weight_decay = .0
    FLAGS.max_grad_norm = .0

  def _test_env(self, z, tvars=None):
    for name in optim._optimizers:
      with self.test_session() as sess:
        FLAGS.optim_name = name
        with tf.variable_scope('optim-' + name):
          train_op = optim.load_optim(10)(z, tvars)
        sess.run(tf.initialize_all_variables())
        yield sess, train_op

  def test_one_step(self):
    x, y, z, k = _get_graph()
    for (sess, train_op) in self._test_env(z):
      [z0, g0] = sess.run([z, optim.get_global_step()])
      sess.run(train_op)
      [z1, g1] = sess.run([z, optim.get_global_step()])
      self.assertLess(z1, z0)
      self.assertEqual(g0+1, g1)

  def test_invalid(self):
    FLAGS.optim_name = "invalid_optim"
    with self.assertRaises(KeyError):
      optim.load_optim(10)

  def test_tvars(self):
    x, y, z, k = _get_graph()
    for (sess, train_op) in self._test_env(z, [x]):
      x0, y0 = sess.run([x, y])
      sess.run(train_op)
      x1, y1 = sess.run([x, y])
      self.assertEqual(y0, y1)

  def test_var_lr(self):
    x, y, z, k = _get_graph()
    optim.set_var_lr(x, .1)
    for (sess, train_op) in self._test_env(z):
      x0, y0 = sess.run([x, y])
      sess.run(train_op)
      x1, y1 = sess.run([x, y])
      ratio = (x1-x0)/(y1-y0)
      self.assertAllCloseAccordingToType(ratio[0], 0.1, atol=5e-5)

  def test_fix_var(self):
    x, y, z, k = _get_graph()
    optim.set_var_lr(x, 0.)

    # fix var should not have weight decay
    FLAGS.weight_decay = 1.
    optim.set_var_decay(x, 1.)

    for (sess, train_op) in self._test_env(z):
      x0, y0 = sess.run([x, y])
      sess.run(train_op)
      x1, y1 = sess.run([x, y])
      self.assertAllCloseAccordingToType(x0, x1, atol=5e-5)

  def test_var_decay(self):
    x, y, z, k = _get_graph()
    FLAGS.weight_decay = 1.
    optim.set_var_decay(x, 1.)
    optim.set_var_decay(x, 0.)
    for (sess, train_op) in self._test_env(z):
      x0, y0 = sess.run([x, y])
      sess.run(train_op)
      x1, y1 = sess.run([x, y])
      diff_y = y1 - y0
      diff_x = x1 - x0
      diff_xy = diff_y - diff_x
      self.assertAllCloseAccordingToType(diff_xy, x0*FLAGS.base_lr, atol=1e-2)

  def test_grad_clip(self):
    x, y, z, k = _get_graph()
    FLAGS.max_grad_norm = .5
    for (sess, train_op) in self._test_env(z):
      k0 = k.eval()
      sess.run(train_op)
      k1 = k.eval()
      diff = k0-k1
      if FLAGS.optim_name != 'adam':
        # adam behaves differently than x := x - alpha * diff
        self.assertAllCloseAccordingToType(diff[0], FLAGS.base_lr*.5, atol=1e-2)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
