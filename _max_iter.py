#!/usr/bin/env python
"""
given a json, find the largest key
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import json

def main():
  for json_path in sys.stdin:
    json_path = json_path.strip()
    with open(json_path) as reader:
      obj = json.load(reader)
      keys = [int(i) for i in obj.keys()]
      print(json_path, max(keys))
  pass

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
