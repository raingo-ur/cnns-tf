#!/usr/bin/env python
"""
general framework for optimization in tensorflow

1. lr_mult support: VAR_LR: (tvars, lr_mult)
2. weight decay: VAR_DECAY: (tvars, decay_rate_mult)
3. optimizer by name
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
from collections import defaultdict

# optim_name
tf.app.flags.DEFINE_string('optim_name', 'adam',
    """specify type of optimizer""")

# lr_decay_epochs
tf.app.flags.DEFINE_integer('lr_decay_epochs', 1,
     """learning rate decay step""")

# lr_decay_rate
tf.app.flags.DEFINE_float('lr_decay_rate', .9,
     """learning rate decay speed""")

# base_lr
tf.app.flags.DEFINE_float('base_lr', 1e-3,
    """the initial learning rate""")

# weight_decay
tf.app.flags.DEFINE_float('weight_decay', 5e-5,
    """weight decay rate""")

# max_grad_norm
tf.app.flags.DEFINE_float('max_grad_norm', 0.,
    """grad clip""")

# momentum
tf.app.flags.DEFINE_float('momentum', 0.9,
    """momentum for the MomentumOptimizer""")

FLAGS = tf.app.flags.FLAGS

def _l2_loss(var):
  name = var.name.replace('/', '_').replace(":", '_')
  with tf.name_scope(name, values=[var]):
    return tf.reduce_sum(tf.square(var)) * .5

def get_global_step():
  step = tf.get_collection("GLOBAL_STEP")
  if len(step) == 0:
    global_step = tf.Variable(0, trainable=False, name='global_step')
    tf.add_to_collection("GLOBAL_STEP", global_step)
    step = [global_step]
  return step[0]

_optimizers = {'sgd':tf.train.GradientDescentOptimizer,
    'rmsprop': tf.train.RMSPropOptimizer,
    'adam': lambda lr: tf.train.AdamOptimizer(lr, beta1=FLAGS.momentum),
    'momentum': lambda lr: tf.train.MomentumOptimizer(lr, FLAGS.momentum),
    }

def _ensure_var_list(var_list):
  if not isinstance(var_list, list):
    var_list = [var_list]
  assert isinstance(var_list[0], tf.Variable)
  return var_list

_local = defaultdict(list)

def set_var_lr(var_list, lr_mult):
  var_list = _ensure_var_list(var_list)
  _local["VAR_LR"].append((var_list, lr_mult))

def set_var_decay(var_list, decay_mult):
  var_list = _ensure_var_list(var_list)
  _local['VAR_DECAY'].append((var_list,decay_mult))

def load_optim(epoch_size):
  decay_steps = FLAGS.lr_decay_epochs * epoch_size
  global_step = get_global_step()

  # Decay the learning rate exponentially based on the number of steps.
  lr = tf.train.exponential_decay(FLAGS.base_lr,
      global_step,
      decay_steps,
      FLAGS.lr_decay_rate,
      staircase=True)
  tf.summary.scalar(lr.name, lr)

  optimizer = _optimizers[FLAGS.optim_name]
  def get_optim(lr_mult=1.):
    return optimizer(lr*lr_mult)

  # make sure FLAGS has no further effects
  weight_decay = FLAGS.weight_decay
  max_grad_norm = FLAGS.max_grad_norm

  # tvar_grads can be precomputed to make it easy to do clone
  def optimize(loss, target_vars = None):

    if target_vars is None:
      target_vars = tf.trainable_variables()

    var_lrs = _local["VAR_LR"]
    fixed_vars = []
    for tvars, lr_mult in var_lrs:
      if lr_mult <= 0.:
        fixed_vars.extend(tvars)
    fixed_vars = set(fixed_vars)

    # for this loss, only include those reachable to loss
    grads0 = tf.gradients(loss, target_vars)
    for grad, tvar in zip(grads0, target_vars):
      if grad is None:
        fixed_vars.add(tvar)

    weight_decays = defaultdict(list)
    for tvars, decay in _local['VAR_DECAY']:
      tvars = set(tvars) & set(target_vars)
      weight_decays[decay].extend(tvars)

    for decay_mult, tvars in weight_decays.items():
      decay = weight_decay * decay_mult
      if decay <= 0.:
        continue
      with tf.name_scope("weight-decay-%g" % decay, values=tvars):
        l2_loss = [_l2_loss(v) for v in tvars
            if v not in fixed_vars]
        if len(l2_loss) == 0:
          continue
        l2_loss = tf.add_n(l2_loss)
        loss += l2_loss * decay

    grads = tf.gradients(loss, target_vars)

    if max_grad_norm > 0.:
      grads = [tf.clip_by_norm(t, max_grad_norm)
          if t is not None else t
          for t in grads]

    tvar_grad = {tvar:grad for tvar, grad in zip(target_vars, grads)}

    for var, grad in tvar_grad.items():
      if grad is not None:
        tf.summary.histogram(var.op.name + '/gradients', grad)
        tf.summary.histogram(var.op.name + '/var', var)

    lr_map = defaultdict(list)

    for _tvars, lr_mult in var_lrs:
      _tvars = [var for var in _tvars if var in target_vars]
      lr_map[lr_mult].extend([(var, tvar_grad[var]) for var in _tvars])
      for var in _tvars:
        tvar_grad.pop(var)

    # default is 1
    lr_map[1.].extend(tvar_grad.items())
    first = True
    train_ops = []
    for lr_mult, tvar_grads in lr_map.items():
      if lr_mult <= .0:
        continue
      grad_tvar = [(f[1],f[0]) for f in tvar_grads if f[1] is not None]
      if len(grad_tvar) == 0:
        continue

      opt = get_optim(lr_mult)
      train_op = opt.apply_gradients(grad_tvar,
          global_step = global_step if first else None)
      first = False
      train_ops.append(train_op)

    return tf.group(*train_ops)

  return optimize

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
