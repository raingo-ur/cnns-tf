#!/usr/bin/env python
"""
some simple ops
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
from . import optim
FLAGS = tf.app.flags.FLAGS

def fc_layer(bottom, nout, name, decay=1., bias=True):
  dim = bottom.get_shape().as_list()[-1]
  with tf.variable_scope(name):
    w = tf.get_variable("weights", [dim, nout])
    top = tf.matmul(bottom, w)
    if bias:
      b = tf.get_variable("biases", [nout])
      top += b
    optim.set_var_decay(w, decay)
  return top

def cross_entropy(logits, label_map, weights=None, name='cross-entropy'):
  with tf.name_scope(name, values=[label_map, logits]):
    xent = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,
        labels=label_map)

    if weights is not None:
      xent = xent * weights

    return tf.reduce_sum(xent, 1)

def _get_dirs():
  model_dir = osp.dirname(FLAGS.ckpt_path)
  root_dir = model_dir
  while len(root_dir) > 1 and not osp.exists(osp.join(root_dir, '_root')):
    root_dir = osp.dirname(root_dir)
  data_dir = osp.dirname(FLAGS.data_dir)
  dir_names = ['model', 'root', 'data', 'abs']
  return {'model':model_dir, 'root':root_dir, 'data':data_dir, 'abs':""}

def resolve_path(path):
  dirs = _get_dirs()
  fields = path.split(':')

  if len(fields) == 1:
    fields = ['model', path]

  this_dir = dirs[fields[0]]
  path = fields[1]
  path = osp.join(this_dir, path)
  path = osp.realpath(path)
  return path

"""
Multi dimensional softmax,
refer to https://github.com/tensorflow/tensorflow/issues/210

compute softmax along the dimension of target
the native softmax only supports batch_size x dimension
"""
def softmax(target, axis, name=None):
  with tf.name_scope(name, 'softmax', values=[target]):
    max_axis = tf.reduce_max(target, axis, keep_dims=True)
    target_exp = tf.exp(target-max_axis)
    normalize = tf.reduce_sum(target_exp, axis, keep_dims=True)
    softmax = target_exp / normalize
    return softmax

def _ensure_list(kvs):
  for k, v in kvs.items():
    if not isinstance(v, (list, tuple)):
      kvs[k] = [1, v, v, 1]

def locally_connected(images, ksizes, nout,
    bias=True, weights=None,
    strides=1, rates=1, padding='VALID', trainable=True,
    weights_initializer=None, biases_initializer=None):
  with tf.variable_scope('LocallyConnected', values=[images]):
    kwargs = {}
    kwargs['ksizes'] = ksizes
    kwargs['strides'] = strides
    kwargs['rates'] = rates
    _ensure_list(kwargs)
    kwargs['padding'] = padding

    # NxRxCxI
    patches = tf.extract_image_patches(images, **kwargs)
    N, R, C, I = patches.get_shape().as_list()

    # RxCxNxI
    patches = tf.transpose(patches, [1, 2, 0, 3])

    if weights is None:
      # RxCxIxO
      weights = tf.get_variable("weights",
          [R, C, I, nout], trainable=trainable,
          initializer=weights_initializer)

    # RxCxNxO
    outputs = tf.matmul(patches, weights)

    if bias:
      b = tf.get_variable('bias',
          [R, C, 1, nout], trainable=trainable,
          initializer=biases_initializer)
      outputs += b

    # NxRxCxO
    outputs = tf.transpose(outputs, [2,0,1,3])
    return outputs

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
