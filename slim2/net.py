#!/usr/bin/env python
"""
load inception cnn
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf

this_dir = osp.dirname(osp.realpath(__file__))

sys.path.append(osp.join(this_dir, 'tf-models/slim/'))
from nets.nets_factory import get_network_fn

from .. import base

class SlimConfig(object):
  """
  config template for slim based models
  """
  def __init__(self, name, **kwargs):
    self.name = name
    self.feature_dim = 1536
    self.imagenet_num_classes = 1001
    self.logits_name = 'Logits'
    self.feature_name = 'PreLogitsFlatten'
    self.input_image_size = 299
    self._CKPT_PATH = osp.join(this_dir, '../caffe/', 'data', 'slim2', name, 'model.ckpt')
    self.pretrained = True
    for k, v in kwargs.items():
      setattr(self, k, v)

configs = [
    SlimConfig('inception_resnet_v2'),
    SlimConfig('inception_v4'),
    SlimConfig('inception_v1', input_image_size=224, feature_dim=1024),
    ]

class SlimNet(base.CNN):

  def __init__(self, images, is_train=True, **kwargs):
    super(SlimNet, self).__init__(this_dir)
    net_func = get_network_fn(self.name, self.imagenet_num_classes, is_training=is_train)
    with tf.variable_scope(self.name) as scope, \
        tf.name_scope(self.name, values=[images]) as op_scope:
      self.scope = scope
      self.op_scope = op_scope
      _, endpoints = net_func(images, **kwargs)

      # the logits API is mainly for testing purpose
      self.logits = endpoints[self.logits_name]
      self.features = endpoints[self.feature_name]

      self.net = endpoints

    self.all_trainables = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES,
        scope = self.scope.name)

    self.setup_decay()

    self.all_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
        scope = self.scope.name)

    batch_norms = tf.get_collection(
        tf.GraphKeys.UPDATE_OPS,
        self.op_scope)
    self.update_ops = tf.group(*batch_norms)

  # returns the trainable variables
  # and the additional updates needed after opt op
  def trainables(self):
    return self.all_trainables

  def init(self, sess):
    sess.run(tf.variables_initializer(self.all_vars))
    restore_dict = {self.strip_scope(var.name):var
        for var in self.all_vars}
    restorer = tf.train.Saver(restore_dict)
    if osp.exists(self._CKPT_PATH):
      print("restore", self._CKPT_PATH, file=sys.stderr)
      restorer.restore(sess, self._CKPT_PATH)

  @classmethod
  def _rescale(cls, image):
    image = image-0.5
    image = image*2.0
    return image

classes = {}
for config in configs:
  classes[config.name] = type(config.name, (SlimNet,), config.__dict__)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
