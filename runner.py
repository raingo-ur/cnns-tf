#!/usr/bin/env python
"""
the runner logic
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import os
import math
import numpy as np
from collections import defaultdict

import tensorflow as tf
from tensorflow.python.client import timeline
from .monitor import load_monitor, JsonBackend
from .optim import get_global_step, load_optim, set_var_lr
from .ops import resolve_path
FLAGS = tf.app.flags.FLAGS

# depend
tf.app.flags.DEFINE_string('depend', '',
     """specify which file to depend on, if not exists, just exit with error""")

# np_weights
tf.app.flags.DEFINE_string('np_weights', '',
     """loading weigths from numpy file, matching by shape""")

# tracing
tf.app.flags.DEFINE_integer('tracing_burn_in', -1,
    """how many iterations to run before tracing. -1 to turn off tracing""")

# num_epochs
tf.app.flags.DEFINE_float('num_epochs', 50,
     """the number of epochs for training""")

# num_eval_epochs
tf.app.flags.DEFINE_float('num_eval_epochs', 1.,
     """the number of epochs for evaluation""")

# convert_graph_after_done
tf.app.flags.DEFINE_boolean('convert_graph_after_done', False,
     """whether to generate pb file after training is done""")

# eval_only
tf.app.flags.DEFINE_boolean('eval_only', False,
     """whether to perform eval only""")

# graph_path
tf.app.flags.DEFINE_string('graph_path', "",
     """where to dump graph, if non empty. runner exit after graph dumped""")

# load_cnn_weights
tf.app.flags.DEFINE_boolean('load_cnn_weights', False,
     """whether to load pretrained cnn weights""")

# eval_outputs_path
tf.app.flags.DEFINE_string('eval_outputs_path', "",
     """eval outputs path for debug purpose""")

# log_device
tf.app.flags.DEFINE_boolean('log_device', False,
     """whether to log device placement""")

# eval_split
tf.app.flags.DEFINE_string('eval_split', '',
     """the split for evaluation, dev or test""")

# val_step
tf.app.flags.DEFINE_integer('val_step', 1000,
     """control the step between validations""")

# summary_step
tf.app.flags.DEFINE_integer('summary_step', 0,
     """control the step between summary""")

# train_split
tf.app.flags.DEFINE_string('train_split', 'train.tf',
     """the split for evaluation, dev or test""")

# pretrained_ckpt
tf.app.flags.DEFINE_string('pretrained_ckpt', "",
     """the pretrained ckpt path, relative to the CKPT_PATH dir""")

# gpu_mem
tf.app.flags.DEFINE_float('gpu_mem', 0,
    """gpu memory fraction to use""")

# train_batch_size
tf.app.flags.DEFINE_integer('train_batch_size', 256,
    """training batch size""")

# eval_batch_size
tf.app.flags.DEFINE_integer('eval_batch_size', 50,
    """evaluation batch size""")

# cnn_lr_mult
tf.app.flags.DEFINE_float('cnn_lr_mult', 1.,
    """learning rate for the CNN part""")

import progressbar
def get_pbar(maxv, param=None):
  progress = progressbar.ProgressBar(max_value=maxv).start()
  if param is not None:
    widgets = progress.widgets
    widgets.append(' ')
    widgets.append(progressbar.widgets.DynamicMessage(param))
    progress = progressbar.ProgressBar(max_value=maxv, widgets=widgets).start()
  return progress

def _init_from_np(sess, np_path):
  # init from numpy weight file, matching by name and shape
  kvs = np.load(np_path).item()
  variables = tf.all_variables()
  num_vars = len(variables)

  assign_ops = []
  count = 0
  for k0, vs in kvs.items():
    for k1, v in vs.items():
      count += 1
      for v_tf in variables:
        if k0 in v_tf.name and \
            k1 in v_tf.name and \
            'optimize' not in v_tf.name  and \
            v_tf.get_shape().as_list() == list(v.shape):
          assign_ops.append(v_tf.assign(v))
          print(k0, k1, "==>", v_tf.name)
  print("Assign", len(assign_ops), "variables from",
      np_path, "out of", num_vars, " variables and", count, "sources")
  sess.run(assign_ops)

def _partial_ckpt(sess):
  path = FLAGS.pretrained_ckpt
  if len(path) == 0:
    return
  path = resolve_path(path)
  assert osp.exists(path + '.index'), "partial_ckpt: %s not found" % path

  reader = tf.train.NewCheckpointReader(path)
  shape_map = reader.get_variable_to_shape_map()
  all_vars = tf.global_variables()
  matched = []
  for var in all_vars:
    name = var.name.split(':')[0]
    if 'global_step' in name:
      # always avoid read global step from pretrained ckpt
      continue
    if name in shape_map and shape_map[name] == var.get_shape().as_list():
      matched.append(var)

  print('loading', len(matched), 'variables from', path)
  saver = tf.train.Saver(matched)
  saver.restore(sess, path)

def _one_train_iter(name, _get_iter):
  with tf.variable_scope(name):
    train_iter = _get_iter(FLAGS.train_split, True, FLAGS.train_batch_size)
    epoch_size = train_iter[0]
    loss_list = train_iter[1]

    if len(train_iter) > 2 and train_iter[2] is not None:
      cnn = train_iter[2]
      cnn_tvars = cnn.trainables()
      cnn_update = cnn.update_ops
      set_var_lr(cnn_tvars, FLAGS.cnn_lr_mult)
    else:
      cnn_update = None
      cnn_tvars = None
      cnn = None

  with tf.name_scope('optim'):
    optim = load_optim(epoch_size)

  def _is_subset(lhs, rhs):
    return rhs is None or len(set(lhs) - set(rhs)) == 0

  optim_list = []
  for idx, (loss, mix, tvars) in enumerate(loss_list):
    with tf.name_scope("optimize-%02d" % idx, values=[loss]):
      train_op = optim(loss, tvars)
      if cnn_update is not None and _is_subset(cnn_tvars, tvars):
        train_op = tf.group(train_op, cnn_update)
      optim_list.append((train_op, loss, mix))

  return optim_list, epoch_size, cnn

def _setup_train(name, _get_iter):

  optim_list, epoch_size, cnn = _one_train_iter(
      name, _get_iter)

  mix_array = [f[2] for f in optim_list]
  total = sum(mix_array)
  mix_array = [mix/total for mix in mix_array]
  mix_cum = np.array(mix_array).cumsum()

  def _train(sess, run_options=None, run_metadata=None):
    goal = mix_cum.searchsorted(np.random.sample())
    train_op, loss, _ = optim_list[goal]
    res = sess.run([loss, train_op],
        options=run_options,
        run_metadata=run_metadata)[0]
    return res

  def _init_cnn(sess):
    if cnn is None:
      return
    assert cnn.pretrained, "to load_cnn_weights, the cnn has to be pretrained"
    cnn.init(sess)

  return _train, _init_cnn, epoch_size

def _setup_eval(name, _get_iter):

  with tf.variable_scope(name, reuse=None if FLAGS.eval_only else True):
    epoch_size, outputs_T, eval_func = _get_iter(FLAGS.eval_split, False,
        FLAGS.eval_batch_size)

  def _eval(sess):

    outputs = [[] for _ in range(len(outputs_T))]
    print("\neval ...")

    eval_iters = int(epoch_size * FLAGS.num_eval_epochs)
    progress = get_pbar(eval_iters)

    for cnt in range(eval_iters):
      _outputs = sess.run(outputs_T.values())
      for o, _o in zip(outputs, _outputs):
        o.append(_o)

      progress.update(cnt)

    outputs_cat = {}
    for name, o in zip(outputs_T, outputs):
      outputs_cat[name] = np.concatenate(o, axis=0)

    if len(FLAGS.eval_outputs_path) > 0:
      output_path = resolve_path(FLAGS.eval_outputs_path)
      output_path += "." + FLAGS.eval_split + '.npz'
      np.savez(output_path, **outputs_cat)
      sys.exit(0)
    return eval_func(outputs_cat)

  return _eval

def _main(sess, name, _get_iter):

  # for backward compatibility to make eval-only work properly
  with tf.name_scope('train/optim'):
    # activate global step
    step = get_global_step()

  if not FLAGS.eval_only:
    with tf.name_scope("train"):
      train_func, init_cnn, epoch_size = _setup_train(name, _get_iter)

  with tf.name_scope("eval"):
    eval_func = _setup_eval(name, _get_iter)

  do_train = 'dev' in FLAGS.eval_split and FLAGS.num_epochs > 0 and not FLAGS.eval_only

  if FLAGS.summary_step > 0 and do_train:
    summary_op = tf.summary.merge_all()
    summary_dir = resolve_path('model:summary-%s' % FLAGS.eval_split)
    if not osp.exists(summary_dir):
      os.makedirs(summary_dir)

    summary_writer = tf.summary.FileWriter(summary_dir, sess.graph)

  sess.run(tf.local_variables_initializer())

  restored, monitor = load_monitor(eval_func, sess, FLAGS.eval_split)
  if restored:
    print('model loaded from snapshot')
  else:
    sess.run(tf.global_variables_initializer())
    if FLAGS.load_cnn_weights and not FLAGS.eval_only:
      init_cnn(sess)

    _partial_ckpt(sess)

    if osp.exists(FLAGS.np_weights):
      _init_from_np(sess, FLAGS.np_weights)

  coord = tf.train.Coordinator()
  threads = tf.train.start_queue_runners(sess=sess, coord=coord)

  tf.get_default_graph().finalize()

  if len(FLAGS.graph_path) > 0:
    with open(FLAGS.graph_path, 'w') as writer:
      writer.write(str(sess.graph_def))
    sys.exit(-1)

  global_step = get_global_step()
  step = global_step.eval(session=sess)

  if FLAGS.log_device:
    return

  if do_train:
    max_iters = FLAGS.num_epochs * epoch_size
    max_iters = int(max_iters)

    jb = JsonBackend(FLAGS.train_split)
    progress = get_pbar(max_iters, 'loss')
    for step in range(step, max_iters):
      tracing = FLAGS.tracing_burn_in > 0 and step > FLAGS.tracing_burn_in

      if tracing:
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
      else:
        run_options = None
        run_metadata = None

      if FLAGS.tracing_burn_in < 0 and step % FLAGS.val_step == 0:
        if not monitor(step):
          break
        progress = get_pbar(max_iters, 'loss')

      if FLAGS.summary_step > 0 and step % FLAGS.summary_step == 0:
        summary_str = sess.run(summary_op)
        summary_writer.add_summary(summary_str, step)

      _loss = train_func(sess, run_options, run_metadata)

      graph_save_path = FLAGS.ckpt_path + '.pbbin'
      if not osp.exists(graph_save_path) \
          and FLAGS.convert_graph_after_done:
        print("saving graph binary:", graph_save_path, file=sys.stderr)
        with open(graph_save_path, 'wb') as writer:
          # remove the ops in imported ops
          # the actual ops are in the imported_1 scope
          # 1. not used in inference
          # 2. causing problems if loss scope has imported ops
          gd = sess.graph_def
          clean_gd = tf.GraphDef()
          bl_list = ['imported/']
          clean_gd.node.extend([op for op in gd.node
            if all([bl not in op.name for bl in bl_list])])
          writer.write(clean_gd.SerializeToString())

      if tracing:
        tl = timeline.Timeline(run_metadata.step_stats)
        ctf = tl.generate_chrome_trace_format()
        with open('trace.json', 'w') as writer:
          writer.write(ctf)
        raise Exception("Tracing Done: trace.json")

      if math.isnan(_loss):
        print("[WARNING] NaN loss:", step, file=sys.stderr)
        break
      progress.update(step, loss=_loss)
      jb.infos[str(step)] = float(_loss)

  monitor(step, do_train)

  coord.request_stop()
  coord.join(threads)

def _try_fetch_dependency(path):
  import subprocess
  if osp.exists(path):
    return

  model_dir = osp.dirname(path)
  this_dir = osp.dirname(__file__)
  subprocess.call(['%s/gcs-download.sh' % this_dir, model_dir, 'yes'])

def main(name, _get_iter, allow_unkonw_flags=False):
  if not allow_unkonw_flags:
    # FIXME hackly way to error on unknown flags
    tf.app.flags._global_parser.parse_args()

  if len(FLAGS.depend) > 0:
    path = resolve_path(FLAGS.depend)
    _try_fetch_dependency(path)
    if not osp.exists(path):
      print("missing dependency:", path, file=sys.stderr)
      sys.exit(-1)

  flags_path = 'model:FLAGS.' + FLAGS.eval_split
  with open(resolve_path(flags_path), 'w') as writer:
    for k, v in FLAGS.__flags.items():
      v = str(v)
      if len(v) == 0:
        v = '<empty>'
      print(k, "=", str(v), file=writer)

  with tf.Session(
      config=tf.ConfigProto(
      intra_op_parallelism_threads=1,
      log_device_placement=FLAGS.log_device,
      allow_soft_placement=True,
      gpu_options = tf.GPUOptions(
        per_process_gpu_memory_fraction=FLAGS.gpu_mem
        ))) as sess:
    _main(sess, name, _get_iter)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
