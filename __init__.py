#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

from . import caffe
from . import slim2

from .base import CNN
import tensorflow as tf
FLAGS = tf.app.flags.FLAGS

# cnn_class
tf.app.flags.DEFINE_string('cnn_class', 'caffe.caffenet',
     """the CNN class to use""")

def load_cnn(cnn_name=None):
  if cnn_name is None:
    cnn_name = FLAGS.cnn_class
  return eval(cnn_name).Net

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
