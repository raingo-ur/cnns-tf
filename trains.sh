#!/bin/bash
# vim ft=sh

## experiment discover: any config file in the exp_dir is treated as an experiments
## for the hiearchy config system, only the leaf node should have the name "config"
## other places use _config

#exp_dir=data/exps/baseline/

this_dir=`dirname $0`
real_this_dir=`readlink -f $0`
real_this_dir=`dirname $real_this_dir`

exps_dir=$1
shift
gpu=$1
shift
args=$@

if [ ! -d "$exps_dir" ]
then
  echo "$exps_dir not found"
  exit
fi

function _count {
  find $exps_dir -name 'FINISHED' | wc -l
}

function _cfgs {
  find $exps_dir -name 'config'
}
_cfgs

prev=-1
cnt=0
while [ "$prev" -lt "$cnt" ]
do
  for exp_dir in `_cfgs | xargs -L 1 dirname | sort -u | sort -R`
  do
    $this_dir/train.sh $exp_dir $gpu $args
  done
done && python $real_this_dir/email_notify.py "trains.sh DONE $exps_dir $gpu $args "
