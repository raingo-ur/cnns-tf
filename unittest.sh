#!/bin/bash
# vim ft=sh

export GRAPH_BASE='./datasets/toy/jtree.pb/'
export DATA_DIR='./datasets/toy/'
coverage run --source $PWD -m unittest $@
coverage html
open htmlcov/index.html
