#!/usr/bin/env python
"""
testing setup for vgg16
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import tensorflow as tf
from .net import base, classes

for name, cls in classes.items():
  test_cls = type('Test_%s' % name,
      (base.TestCNN, tf.test.TestCase), {'_CNN':cls})
  globals()[test_cls.__name__] = test_cls

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
