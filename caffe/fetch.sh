#!/bin/bash
# vim ft=sh

save_dir=$HOME/data-bank/tf-cnns-data/caffe
mkdir -p $save_dir
gsutil -m rsync -r $GCS_ROOT/cnns-caffe-data $save_dir
rm -Rf data
ln -s $save_dir data
