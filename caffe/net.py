#!/usr/bin/env python
"""
base class for CaffeNet
interface to caffe-tensorflow
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf
import numpy as np

this_dir = osp.dirname(osp.realpath(__file__))

sys.path.append(osp.join(this_dir, 'data/'))
sys.path.append(osp.join(this_dir, 'caffe-tensorflow/'))

from .. import base

FLAGS = tf.app.flags.FLAGS
# use_caffe_init
tf.app.flags.DEFINE_boolean('use_caffe_init', False,
    """whether to use caffe init, i.e., stddev 0.01""")

class CaffeConfig(object):
  """docstring for CaffeConfig"""
  def __init__(self, name):

    self.mean = np.array([[[104., 117., 124.]]])
    self.name = name
    self.input_image_size = 224
    self.isotropic = True
    self.input_name = 'input'

    self.output_size = 4068
    self.imagenet_num_classes = 1000

    self.scale_size = 256
    self.expect_bgr = True

    self.feature_layer = 'fc7'
    self.feature_dim = 4096

    self.logits_layer = 'fc8'
    self.pretrained = True


def old_config(name):
  config = CaffeConfig(name)
  config.input_name = 'data'
  config.input_image_size = 227
  config.isotropic = False
  return config

def vgg_config(name):
  config = CaffeConfig(name)
  return config

configs = [vgg_config('vgg16'),
    old_config('alexnet'), old_config('caffenet')]

class CaffeNet(base.CNN):

  def __init__(self, images, is_train=True, **kwargs):

    super(CaffeNet, self).__init__(this_dir)

    self._NPY_PATH = osp.join(this_dir, 'data', self.name, '_net.npy')
    NetTrain = __import__('%s._train' % self.name, fromlist = ['Net']).Net
    NetTest = __import__('%s._test' % self.name, fromlist = ['Net']).Net

    initializer = tf.random_normal_initializer(stddev=0.01) \
        if FLAGS.use_caffe_init \
        else None

    with tf.variable_scope(self.name,
        initializer = initializer
        ) as scope:
      if is_train:
        net_type = NetTrain
      else:
        net_type = NetTest

      self.net = net_type({self.input_name:images})
      self.logits = self.net.layers[self.logits_layer]
      self.features = self.net.layers[self.feature_layer]
      self.scope = scope

    self.all_trainables = tf.get_collection(
        tf.GraphKeys.TRAINABLE_VARIABLES,
        scope = self.scope.name)
    self.setup_decay()
    self.update_ops = tf.no_op()

  # returns the trainable variables
  # and the additional updates needed after opt op
  def trainables(self, train_cnn=False):
    return self.all_trainables

  def init(self, sess):
    all_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
        scope = self.scope.name)
    sess.run(tf.variables_initializer(all_vars))
    with tf.variable_scope(self.scope.name, reuse=True):
      self.net.load(data_path=self._NPY_PATH, session=sess)
    print("initialized caffe net from", self._NPY_PATH, file=sys.stderr)

  @classmethod
  def _rescale(cls, image):
    if cls.expect_bgr:
      image = tf.reverse(image, [2])
    image = image * 255 - cls.mean
    return image

classes = {}
for config in configs:
  classes[config.name] = type(config.name, (CaffeNet,), config.__dict__)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
