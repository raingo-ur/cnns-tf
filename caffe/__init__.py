#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

class _Net(object):
  def __init__(self, Net):
    self.Net = Net

from .net import classes
for name, cls in classes.items():
  globals()[name] = _Net(cls)

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
