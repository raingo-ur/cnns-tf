#!/bin/bash
# vim ft=sh

CAFFE_TOOLS=$CAFFE_ROOT/build/tools

U_BIN=$CAFFE_TOOLS/upgrade_net_proto_binary
if [ ! -f "$U_BIN" ]
then
  echo $U_BIN is not found in CAFFE_ROOT
  exit
fi

U_TXT=$CAFFE_TOOLS/upgrade_net_proto_text
if [ ! -f "$U_BIN" ]
then
  echo $U_BIN is not found in CAFFE_ROOT
  exit
fi

PYPATH=$CAFFE_ROOT/python/
export PYTHONPATH=$PYPATH
caffe_version=`python -c "import caffe; print caffe.__version__"`
if [ -z "$caffe_version" ]
then
  echo pycaffe is not found in CAFFE_ROOT
  exit
fi

tf_version=`python -c "import tensorflow as tf; print tf.__version__"`
if [ -z "$tf_version" ]
then
  echo can not import tensorflow
  exit
fi

mkdir -p data
cd data

function fetch {

cnn=$1
WEIGHTS_URL=$2
PBTXT_URL=$3

mkdir -p $cnn
touch $cnn/__init__.py

if [ -z "$WEIGHTS_URL" ]
then
  echo WEIGHTS_URL is undefined
  exit
fi

if [ -z "$PBTXT_URL" ]
then
  echo PBTXT_URL is undefined
  exit
fi

cd $cnn

if [ ! -f net.pbtxt ]
then
  curl $PBTXT_URL | sed 's/^name: .*/name: "Net"/' | $U_TXT /dev/stdin net.pbtxt
fi

if [ ! -f net.weights ]
then
  curl $WEIGHTS_URL | $U_BIN /dev/stdin net.weights
fi

python ../../caffe-tensorflow/convert.py net.pbtxt --caffemodel net.weights --code-output-path _test.py --data-output-path /dev/null
python ../../caffe-tensorflow/convert.py net.pbtxt --caffemodel net.weights --code-output-path _train.py -p train --data-output-path _net.npy

cd ..

}

#cnn=local
#WEIGHTS_URL=file:///run/media/yli/yli-seagate2/gpu-cs3/mll-exps/configs/scratch/exp.define-exps/nus-wide/model_iter_29296.caffemodel
#PBTXT_URL=file:///run/media/yli/yli-seagate2/gpu-cs3/mll-exps/configs/scratch/exp.define-exps/nus-wide/deploy.prototxt
#fetch $cnn $WEIGHTS_URL $PBTXT_URL
#exit

cnn=alexnet
WEIGHTS_URL=http://dl.caffe.berkeleyvision.org/bvlc_alexnet.caffemodel
PBTXT_URL=https://raw.githubusercontent.com/BVLC/caffe/master/models/bvlc_alexnet/deploy.prototxt
fetch $cnn $WEIGHTS_URL $PBTXT_URL

cnn=caffenet
WEIGHTS_URL=http://dl.caffe.berkeleyvision.org/bvlc_reference_caffenet.caffemodel
PBTXT_URL=https://raw.githubusercontent.com/BVLC/caffe/master/models/bvlc_reference_caffenet/deploy.prototxt
fetch $cnn $WEIGHTS_URL $PBTXT_URL

cnn=vgg16
WEIGHTS_URL=http://www.robots.ox.ac.uk/~vgg/software/very_deep/caffe/VGG_ILSVRC_16_layers.caffemodel
PBTXT_URL=https://gist.githubusercontent.com/ksimonyan/211839e770f7b538e2d8/raw/ded9363bd93ec0c770134f4e387d8aaaaa2407ce/VGG_ILSVRC_16_layers_deploy.prototxt
fetch $cnn $WEIGHTS_URL $PBTXT_URL

gsutil -m rsync -r $PWD/data/ $GCS_ROOT/cnns-caffe-data/
