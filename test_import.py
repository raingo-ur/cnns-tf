#!/usr/bin/env python
"""
Code description
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp

import tensorflow as tf

from . import load_cnn, CNN

names = ['caffe.vgg16', 'caffe.alexnet', 'caffe.caffenet', 'slim2.inception_resnet_v2']

class TestImport(tf.test.TestCase):
  pass

def _test_import(self, name):
  Net = load_cnn(name)
  self.assertTrue(isinstance(Net, CNN))

def _test_init(self, name):
  Net = load_cnn(name)
  self.assertEqual(len(Net.classify_jpeg()), 3)

for func in [_test_import, _test_init]:
  for name in names:
    setattr(TestImport,
        'test' + func.__name__ + '_' + name.replace('.', '_'),
        lambda s: func(s, name))

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
