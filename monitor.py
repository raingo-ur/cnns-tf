#!/usr/bin/env python
"""
given a callback, update to determine whether to save to snapshot, and manging checkpoints
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
__author__ = "Raingo Lee (raingomm@gmail.com)"

import sys
import os.path as osp
import tensorflow as tf

# ckpt_path
tf.app.flags.DEFINE_string('ckpt_path', './data/samples/model.ckpt',
     """checkpoint path""")

FLAGS = tf.app.flags.FLAGS

import json
class JsonBackend(object):
  def __init__(self, split):
    self.info_path = FLAGS.ckpt_path + '.' + split + '.json'
    try:
      with open(self.info_path) as reader:
        self.infos = json.load(reader)
    except Exception as ioe:
      print("loading previous json error:", str(ioe), file=sys.stderr)
      self.infos = {}

  def __del__(self):
    with open(self.info_path, 'w') as writer:
      json.dump(self.infos, writer, indent=4)

def load_monitor(callback, sess, split):
  saver = tf.train.Saver(tf.global_variables())

  restored = False
  jb = JsonBackend(split)

  ckpt_path = FLAGS.ckpt_path
  ckpt_path_new = FLAGS.ckpt_path + '.snapshot'

  if osp.exists(ckpt_path_new + '.index'):
    saver.restore(sess, ckpt_path_new)
    restored = True
  elif osp.exists(ckpt_path + '.index'):
    saver.restore(sess, ckpt_path)
    restored = True

  def best_so_far():
    info = max(jb.infos.items(), key=lambda x:x[1]['score'])
    return info[1]['score']

  def update(step, do_save=True):
    info, score = callback(sess)

    if do_save:
      saver.save(sess, ckpt_path_new, write_meta_graph=False)
      if len(jb.infos) == 0 or score > best_so_far():
        saver.save(sess, ckpt_path, write_meta_graph=False)

    step = str(step)
    jb.infos[step] = {}
    jb.infos[step]['info'] = info
    jb.infos[step]['score'] = score
    print("\nscore:", score)

    # no early terminate at this point
    return True

  return restored, update

def main():
  raise NotImplemented("Not tested")

if __name__ == "__main__":
  main()

# vim: tabstop=4 expandtab shiftwidth=2 softtabstop=2
